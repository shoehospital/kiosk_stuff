**Kiosk Management Folder**

Use this directory to manage anything related to the Kiosk Management. 

* Kiosk Management Server: Hosted on Vultr. * 
     FQDN: kioskmgmt.mshdealer.com
	 IP: 149.28.243.148
	 VNC and SSH Availabe
	 Need to review Firewall Rules / Best Practices

---

## Maniuplating ISO Images

Use this section to understand how modules are prepared. 

# Decompress ISO: 
    mkdir /tmp/kiosk_ISO
    mount -o loop kiosk.iso /mnt/cdrom
    cp -a /mnt/cdrom/* /tmp/kiosk_ISO
    umount /mnt/cdrom
	cd /tmp/kiosk_ISO

# Open/Edit existing module (from .xzm): 
1. From ISO Location (/tmp/kiosk_ISO) move the module to a new location and then unsquash (my /tmp/kiosk_ISO/xzm/002-firefox.xzm /tmp/)
	# unsquashfs /tmp/002-firefox.xzm (This requires the unquashfs module to be installed. If it is not available, you will need to download it from the package manager. 
2. A folder with files contained within will be decompressed. (/tmp/squashfs-root) 
3) cd into the folder and you will see a directory tree that matches the server file structure. (Not every filesystem will be represented)
     # ls -ltr squashfs-root/
		drwxr-xr-x 4 root root 4096 Jun 11  2016 var
		drwxr-xr-x 7 root root 4096 Jun 11  2016 usr
		drwxr-xr-x 6 root root 4096 May 12 11:54 etc
		drwxr-xr-x 3 root root 4096 Jul  8 13:19 home
4) Make required updates or modifications and cd back to top layer

# Compress existing module (back to .xzm)
1. From top folder layer (/tmp/squashfs-root) (/tmp/fakeroot) execute the following: 
  	# mksquashfs /tmp/fakeroot /tmp/customsettings.xzm -comp xz -b 256K -Xbcj x86 -noappend
	# mksquashfs /tmp/squashfs-root/ /tmp/squid2.xzm -comp xz -b 256K -Xbcj x86 -noappend
2. Copy the newly compressed module file into the ISO Image: 
	# cp -a squid2.xzm /tmp/kiosk_ISO/xzm/

# Generate ISO Image
1. From the ISO Root Folder --> cd /tmp/kiosk_ISO 
2. Execute the Make_iso.sh script using bash
	# bash make_iso.sh
3. If this job fails, you may need to install mkisofs from your package manager (On Ubuntu 20.04 LTS this was required) 
4. Your ISO will now be available in the /tmp/ directory as /tmp/Porteus-Kiosk.iso
Note: This appeared to cause kiosk failure when run from a shell prompt instead of directly from the console. 

# Burn the ISO on non-optical storage media
1. Since we are using jump drives for the installation media, we need to create a ISOHybrid file. 
2. If isohybrid is not installed, you may need to install syslinux and syslinux-utils from your package manager (On Ubuntu 20.04 LTS this was required) 
3. # isohybrid /tmp/Porteus-Kiosk.iso (the -u flag should be used when UEFI is required. Our kiosks do not use UEFI)

# Copy the ISO to USB Media: 
1. If you are using a jump drive, identify the drive name (fdisk) or df -h if the drive is mounted. 
    # dd if=Porteus-Kiosk.iso of=/dev/sdb

Porteus Kiosk Site Links: 
  - https://porteus-kiosk.org/kiosk-customization.html
  - https://porteus-kiosk.org/modules.html 
  ** Porteus Kiosk Public Repo: https://porteus-kiosk.org/public/ ** 

---



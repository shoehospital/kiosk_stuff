#!/bin/sh

# Stop Squid:
if [ "`pidof squid`" ]; then
    killall squid
    # Wait at least one minute for it to exit, as we don't know how big the DB is:
    for second in 0 1 2 3 4 5 6 7 8 9; do
        [ "`pidof squid`" ] || { echo "[1;33mStopped:[0m squid"; break; }
        sleep 1
    done
    if [ "$second" = "10" ]; then
      echo "[31mWARNING:[0m Gave up waiting for squid to exit!"
      sleep 15
    fi
fi

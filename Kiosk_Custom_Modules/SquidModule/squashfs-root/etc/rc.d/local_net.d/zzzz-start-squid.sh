#!/bin/sh

if [ -z "`pidof squid`" ]; then
    # Setup procedure:
    echo 'squid:x:105:105:added by portage for squid:/var/cache/squid:/sbin/nologin' >> /etc/passwd
    echo 'squid:!:16963::::::' >> /etc/shadow
    echo 'squid:x:105:' >> /etc/group
    mkdir -p /var/log/squid; chown 105:105 /var/log/squid /usr/libexec/squid/pinger /usr/libexec/squid/basic_ncsa_auth

    # Enable cache directory:
    sed -i 's/^#cache_dir /cache_dir /' /etc/squid/squid.conf
    test -d /var/cache/squid/00 || { mkdir -p /var/cache/squid; chown 105:105 /var/cache/squid; squid -z; sleep 3; }

    # Force logging through proxy:
    sed -i 's_^proxy=DIRECT_proxy=127.0.0.1:3128_g' /etc/profile.d/proxy.sh

    # Set visible hostname and shutdown lifetime:
    echo -e "\nvisible_hostname $(hostname)\nshutdown_lifetime 1 seconds" >> /etc/squid/squid.conf

    # Start squid daemon:
    squid
else
    echo "Squid daemon is started already"
fi
